// Javascript Statements:
/*  
    Set of instructions that we tell the machine to perform
    JS statements ends with semicolon (;)
*/
console.log("Hello once more!!!!");

// Variable and Syntax

let hello; // initialize
console.log(hello); // undefined


let firstname = "Bryan"; // declare

let productName = "desktop computer";
console.log(productName);

let productPrice = 189999;
console.log(productPrice);

const pi = 3.14;
console.log(pi);

// Reassigning value

productName = "Laptop na sya";
console.log(productName);

let friend = "Kate";
friend = "Chance";
console.log(friend);

/*
let friend = "Jane";
console.log(friend); duplicate variable name
*/

// Can't reaasigned constant variable

/*
pi = 3.1;
console.log(pi); 
*/

// Reassigning vs Initializing

// initialize = first value
let supplier;
supplier = "Jane Smith Tradings";
console.log(supplier);

// reassignment = existing value before that
supplier = "Zuitt Store";
console.log(supplier);

// var vs let/const

// var declaration is at the topmost
// hoisting behavior
a = 5;
console.log(a); // result 5 
var a;

/*
b = 6;
console.log(a); // result 5
var b;
*/

/* let/const local/global scope */

let outerVariable = "hello"; // global

/*
{
    let innerVariable = "hello inner"; // local
}
*/

console.log(outerVariable);
// console.log(innerVariable);


// Multiple Variable Declaration
let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);


// Data Types
// String 
let country = "Philippines"; 
let city = "Manila City";

console.log(city, country);

// Concatenating Strings

let fullAddress = city + "," + " " + country; // can do it inside console.log func
console.log(fullAddress);

const myName = "Bryan Babila";
const greeting = "Hi I am";
console.log(greeting + " " + myName);

// Escape Character (\) new line
// "\n"
// "\t" for indent tab

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message ='John\'s employees went home early'; // you can use double quote " "
console.log(message);

// Numbers - Integgers / Whole Number

let headcount = 26;
console.log(headcount);

// Decimal Number
let grade = 74.9;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10; //e for exponent
console.log(planetDistance);

let number1 = "1"; // string 1 as result

// Combining text and string
console.log("John's grade last quarter is " + grade);

// Boolean - true or false

let isMarried = false;
let isSingle = true;
console.log("isMarried " + isMarried);
console.log("isSingle " + isSingle);

// Arrays
/*
    Syntax: let/const arrayName = [elementA, elementB];
*/

let grades = [98.7, 77.3, 90.8, 88.4];
console.log(grades);

const anime = ["BNHA", "AOT", "SxF", "KNY"];
console.log(anime);

let random = ["JK", 24, true];
console.log(random);

// Object Data Type

/*
    Syntax: 
    
    let/const objectName = {
        propertyA: valueA,
        propertyB: valueB
    }
*/

let person = {
    fullName: "Chelsea",
    age: 35,
    isMarried: false,
    contact: ["09232545878", "8123 4567"],
    address: {
        houseNumber: '345',
        city: 'Manila'
    }
};

console.log(person); // print the property in Alphabetical

const myGrades = {
    firstGrading: 98.7,
    secondGrading: 77.3,
    thirdGrading: 88.6,
    fourthGrading: 74.9
};

console.log(myGrades);

// TYPE OF OPERATOR

console.log(typeof myGrades); // result object - tells the datatype of variable
console.log(typeof grades);
console.log(typeof grade);

/*
anime = ["One Punch Man"]; // when anime is constant result err
console.log(anime);
*/

anime[0] = ['One punch man'];
console.log(anime); 

// Null 
let spouse = null;
console.log(spouse);

// Undefined
let y;
console.log(y);